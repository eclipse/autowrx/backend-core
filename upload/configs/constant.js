module.exports = {
  DIR_IS_NOT_ALLOWED: 'dir is not allowed',
  INVALID_PATH: 'Invalid path or containPath',
  EXISTED_FILE: 'File already exists',
  MISSING_PATH: 'Missing path',
  INVALID_CONTAIN_PATH: 'Invalid containPath',
  PATH_OUTSIDE: 'Path is not in project',
  INVALID_FILES: 'Invalid files',
  MISSING_FILE: 'Missing file',
};
