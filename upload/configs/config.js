module.exports = {
  // appDataDirs: ['be-studio', 'user-mgmt', 'store-be', 'slide'],
  imgExts: ['.jpg', '.png', '.jpeg'],
  thumbnailSize: 256,
  chunkSize: 10 ** 6,
  maxImageSize: 2 * 1024 * 1024,
  rootDataDir: './data/',
};
